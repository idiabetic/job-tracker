require 'rails_helper'

describe Job do
  describe "validations" do
    context "invalid attributes" do
      it "is invalid without a title" do
        category = Category.create(title: 'Banana')
        job = Job.new(level_of_interest: 80, description: "Wahoo", city: "Denver", category_id: category.id)
        expect(job).to be_invalid
      end

      it "is invalid without a level of interest" do
        category = Category.create(title: 'Banana')
        job = Job.new(title: "Developer", description: "Wahoo", city: "Denver", category_id: category.id)
        expect(job).to be_invalid
      end

      it "is invalid without a city" do
        category = Category.create(title: 'Banana')
        job = Job.new(title: "Developer", description: "Wahoo", level_of_interest: 80, category_id: category.id)
        expect(job).to be_invalid
      end
      it "is invalid without a city" do
        category = Category.create(title: 'Banana')
        job = Job.new(title: "Developer", description: "Wahoo", level_of_interest: 80, category_id: category.id)
        expect(job).to be_invalid
      end
    end
    context "valid attributes" do
      it "is valid with a title, level of interest, and company" do
        company = Company.new(name: "Turing")
        category = Category.create(title: 'Banana')
        job = Job.new(title: "Developer", level_of_interest: 40, city: "Denver", company: company, category_id: category.id)
        expect(job).to be_valid
      end
    end
  end
  describe "relationships" do
    it "belongs to a company" do
      job = Job.new(title: "Software", level_of_interest: 70, description: "Wahooo")
      expect(job).to respond_to(:company)
    end
  end
  describe "class methods" do
    scenario "a user sees all jobs sorted by interest level" do
      company = Company.create!(name: "ESPN")
      category = Category.create(title: 'banana')
      job_1 = company.jobs.create!(title: "Developer", level_of_interest: 70, city: "Denver", category: category)
      job_2 = company.jobs.create!(title: "QA Analyst", level_of_interest: 50, city: "LA", category: category)
      job_3 = company.jobs.create!(title: "DB Analyst", level_of_interest: 90, city: "New York City", category: category)

      expect(Job.sort_by('interest')).to eq([job_3, job_1, job_2])
      expect(Job.restrict_by_city("Denver")).to eq [job_1]
    end
    scenario "a user sees how many jobs there are by interest on the dashboard" do
      company = Company.create!(name: "ESPN")
      category = Category.create(title: 'banana')
      job_1 = company.jobs.create!(title: "Developer",  level_of_interest: 1, city: "Denver", category: category)
      job_2 = company.jobs.create!(title: "QA Analyst", level_of_interest: 2, city: "LA", category: category)
      job_3 = company.jobs.create!(title: "DB Analyst", level_of_interest: 3, city: "New York City", category: category)
      job_4 = company.jobs.create!(title: "DB", level_of_interest: 3, city: "New York City", category: category)
      ordered_hash = {3=>2, 2=>1, 1=>1}

      expect(Job.number_level_interest).to eq(ordered_hash)
    end
    it "user can see top companies by interest" do
      company = Company.create!(name: "ESPN")
      company_1 = Company.create!(name: "NASA")
      company_2 = Company.create!(name: "MTV")
      category = Category.create(title: 'banana')
      job_1 = company.jobs.create!(title: "Developer",  level_of_interest: 1, city: "Denver", category: category)
      job_2 = company.jobs.create!(title: "QA Analyst", level_of_interest: 1, city: "LA", category: category)
      job_3 = company_1.jobs.create!(title: "Physicist", level_of_interest: 2, city: "New York City", category: category)
      job_4 = company_1.jobs.create!(title: "Astronaut", level_of_interest: 4, city: "New York City", category: category)
      job_5 = company_2.jobs.create!(title: "Producer", level_of_interest: 4, city: "New York City", category: category)
      job_6 = company_2.jobs.create!(title: "Assistant Producer", level_of_interest: 4, city: "New York City", category: category)

      expected_hash = [[company_2, 0.4e1], [company_1, 0.3e1], [company, 0.1e1]]
      expect(Job.top_companies).to eq(expected_hash)
    end
    scenario "a user sees how many jobs there are by interest on the dashboard" do
      company = Company.create!(name: "ESPN")
      category = Category.create(title: 'banana')
      job_1 = company.jobs.create!(title: "Developer",  level_of_interest: 1, city: "Denver", category: category)
      job_2 = company.jobs.create!(title: "QA Analyst", level_of_interest: 2, city: "LA", category: category)
      job_3 = company.jobs.create!(title: "DB Analyst", level_of_interest: 3, city: "New York City", category: category)
      job_4 = company.jobs.create!(title: "DB", level_of_interest: 3, city: "New York City", category: category)
      job_5 = company.jobs.create!(title: "DB", level_of_interest: 3, city: "Denver", category: category)
      job_6 = company.jobs.create!(title: "DB", level_of_interest: 3, city: "New York City", category: category)
      ordered_hash = {"New York City"=>3, "Denver"=>2, "LA"=>1}

      expect(Job.city_by_number).to eq(ordered_hash)
    end
  end
end
