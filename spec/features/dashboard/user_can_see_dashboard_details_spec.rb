require 'rails_helper'

describe "User Can See Dashboard Details" do
  it "user can see job count by level of interest" do
    company = Company.create!(name: "ESPN")
    category = Category.create(title: 'banana')
    company.jobs.create!(title: "Developer",  level_of_interest: 1, city: "Denver", category: category)
    company.jobs.create!(title: "QA Analyst", level_of_interest: 2, city: "LA", category: category)
    company.jobs.create!(title: "DB Analyst", level_of_interest: 3, city: "New York City", category: category)
    company.jobs.create!(title: "DB", level_of_interest: 3, city: "New York City", category: category)

    visit dashboard_index_path

    expect(page).to have_content("level: 1")
    expect(page).to have_content("1 jobs")
    expect(page).to have_content("level: 2")
    expect(page).to have_content("1 jobs")
    expect(page).to have_content("level: 3")
    expect(page).to have_content("2 jobs")
  end
  it "user can see top companies by interest" do
    company = Company.create!(name: "ESPN")
    company_1 = Company.create!(name: "NASA")
    company_2 = Company.create!(name: "MTV")
    company_3 = Company.create!(name: "FIFA")
    category = Category.create(title: 'banana')
    company.jobs.create!(title: "Developer",  level_of_interest: 1, city: "Denver", category: category)
    company.jobs.create!(title: "QA Analyst", level_of_interest: 1, city: "LA", category: category)
    company_1.jobs.create!(title: "Physicist", level_of_interest: 2, city: "New York City", category: category)
    company_1.jobs.create!(title: "Astronaut", level_of_interest: 4, city: "New York City", category: category)
    company_2.jobs.create!(title: "Producer", level_of_interest: 4, city: "New York City", category: category)
    company_2.jobs.create!(title: "Assistant Producer", level_of_interest: 4, city: "New York City", category: category)
    company_3.jobs.create!(title: "Assistant Producer", level_of_interest: 0, city: "New York City", category: category)

    visit dashboard_index_path

    expect(page).to have_content(company_2.name)
    expect(page).to have_content("4.0 Stars")
    expect(page).to have_content(company.name)
    expect(page).to have_content("3.0 Stars")
    expect(page).to have_content(company_1.name)
    expect(page).to have_content("1.0 Stars")
    expect(page).to_not have_content(company_3.name)
  end
  it 'user can see city listed by number of jobs in each city' do
    company = Company.create!(name: "ESPN")
    category = Category.create(title: 'banana')
    job_1 = company.jobs.create!(title: "Developer",  level_of_interest: 1, city: "Denver", category: category)
    job_2 = company.jobs.create!(title: "QA Analyst", level_of_interest: 2, city: "LA", category: category)
    job_3 = company.jobs.create!(title: "DB Analyst", level_of_interest: 3, city: "New York City", category: category)
    company.jobs.create!(title: "DB", level_of_interest: 3, city: "New York City", category: category)
    company.jobs.create!(title: "DB", level_of_interest: 3, city: "Denver", category: category)
    company.jobs.create!(title: "DB", level_of_interest: 3, city: "New York City", category: category)

    visit dashboard_index_path

    expect(page).to have_link(job_3.city)
    expect(page).to have_link(job_2.city)
    expect(page).to have_link(job_1.city)
    expect(page).to have_content('3 Jobs')
    expect(page).to have_content('2 Jobs')
    expect(page).to have_content('1 Jobs')
  end
end
