require 'rails_helper'

describe 'Category Show Page' do
  before(:each) do
    @category = Category.create!(title: 'Server')
    @company = Company.create!(name: "ESPN")
    @company_2 = Company.create!(name: "Newsy")
    @category = Category.create(title: 'banana')
    @job_1 = @company.jobs.create!(title: "Developer", level_of_interest: 70, city: "Denver", category: @category)
    @job_2 = @company_2.jobs.create!(title: "QA Analyst", level_of_interest: 70, city: "New York City", category: @category)


  end
  it 'should show an individual category' do

    visit category_path(@category)

    expect(page).to have_content(@category.title)
    expect(page).to have_content(@job_1.title)
    expect(page).to have_content(@job_2.title)
  end
  it 'should link to create a new job' do
    visit category_path(@category)

    click_on 'Add New Job'

    expect(current_path).to eq(new_job_path)
  end
  it 'should link to the job' do
    visit category_path(@category)

    click_on 'Developer'

    expect(current_path).to eq(job_path(@job_1))
  end
  it 'should link to the company' do
    visit category_path(@category)

    click_on 'ESPN'

    expect(current_path).to eq(company_path(@company))
  end
  it 'should link to the city' do
    visit category_path(@category)

    click_on 'Denver'
    expect(page).to have_content('Denver Jobs')
  end

end
