require 'rails_helper'

describe 'user can see all categories' do
  before(:each) do
    @category_1 = Category.create(title: "Web Dev")
    @category_2 = Category.create(title: "Sr. Engineer")
    @category_3 = Category.create(title: "Jr. Engineer")
  end
  it 'shows all category titles' do
    visit categories_path

    expect(page).to have_content(@category_1.title)
    expect(page).to have_content(@category_2.title)
    expect(page).to have_content(@category_3.title)
  end
  it 'has an edit button' do
    visit categories_path

    within "#category-#{@category_1.id}-row" do
      expect(page).to have_link('Edit')
      click_on 'Edit'
    end

    expect(current_path).to eq(edit_category_path(@category_1))

    fill_in 'category[title]', with: 'Pancaker'
    click_on('Update Category')
    expect(current_path).to eq(category_path(@category_1))
    expect(page).to have_content(Category.find(@category_1.id).title)
  end
  it 'has a delete button' do
    visit categories_path

    within "#category-#{@category_1.id}-row" do
      expect(page).to have_link('Delete')
      click_on 'Delete'
    end

    expect(current_path).to eq(categories_path)

    expect(page).to_not have_content(@category_1.title)
  end
  it 'displays the number of jobs per category' do
    company = Company.create!(name: 'SEC')
    Category.create(title: 'Bird Person')
    Job.create!(title: 'A Person', level_of_interest: 1, city: 'Denver', category_id: @category_1.id, company: company)
    Job.create!(title: 'Garbage Man', level_of_interest: 1, city: 'Denver', category_id: @category_1.id, company: company)
    Job.create!(title: 'Sailor', level_of_interest: 1, city: 'Denver', category_id: @category_1.id, company: company)
    Job.create!(title: 'Umbrella Spinner', level_of_interest: 1, city: 'Denver', category_id: @category_2.id, company: company)
    Job.create!(title: 'Yellow', level_of_interest: 1, city: 'Denver', category_id: @category_2.id, company: company)
    Job.create!(title: 'blue', level_of_interest: 1, city: 'Denver', category_id: @category_3.id, company: company)


    visit categories_path
    expect(page).to have_content("3 Jobs")
    expect(page).to have_content("2 Jobs")
    expect(page).to have_content("1 Job")
    expect(page).to have_content("0 Jobs")
  end
  it 'links to the category show' do
    company = Company.create!(name: 'SEC')
    Category.create(title: 'Bird Person')
    Job.create!(title: 'A Person', level_of_interest: 1, city: 'Denver', category_id: @category_1.id, company: company)
    Job.create!(title: 'Garbage Man', level_of_interest: 1, city: 'Denver', category_id: @category_1.id, company: company)
    Job.create!(title: 'Sailor', level_of_interest: 1, city: 'Denver', category_id: @category_1.id, company: company)
    Job.create!(title: 'Umbrella Spinner', level_of_interest: 1, city: 'Denver', category_id: @category_2.id, company: company)
    Job.create!(title: 'Yellow', level_of_interest: 1, city: 'Denver', category_id: @category_2.id, company: company)
    Job.create!(title: 'blue', level_of_interest: 1, city: 'Denver', category_id: @category_3.id, company: company)


    visit categories_path

    click_link @category_1.title

    expect(current_path).to eq(category_path(@category_1))
    expect(page).to have_content(@category_1.title)

  end

end
