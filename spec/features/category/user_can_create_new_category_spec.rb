require 'rails_helper'

describe 'user can see individual category' do
  it 'can create new category' do
    visit new_category_path

    expect(page).to have_content("Add New Category")

    fill_in 'category[title]', with:'HR Professional'

    click_on 'Create Category'
    expect(current_path).to eq "/categories/#{Category.maximum(:id)}"
    expect(page).to have_content('HR Professional')
  end
  it 'fails gracefully when the category already exists' do
    category = Category.create(title: 'Package Handler')
    visit new_category_path

    expect(page).to have_content("Add New Category")

    fill_in 'category[title]', with: category.title

    click_on 'Create Category'
    expect(current_path).to eq new_category_path
    expect(page).to have_content("Sorry, #{category.title} already exists")
  end
end
