require 'rails_helper'

describe 'A User can edit a category' do
  it 'should edit a category' do
    category = Category.create(title: "Garbage Man")

    visit categories_path

    within "#category-#{category.id}-row" do
      click_on 'Edit'
    end
    expect(current_path).to eq(edit_category_path(category))

    expect(page).to have_content(category.title)

    fill_in 'category[title]', with: "Apple Picker"
    click_on('Update Category')
    expect(current_path).to eq(category_path(category))
    expect(page).to have_content(Category.find(category.id).title)
  end
end
