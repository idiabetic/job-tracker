require 'rails_helper'

describe "User sees one company" do
  scenario "a user sees a company" do
    company = Company.create!(name: "ESPN")
    category = Category.create(title: 'Banana')
    company.jobs.create!(title: "Developer", level_of_interest: 90, city: "Denver", category: category)

    visit company_path(company)

    expect(current_path).to eq("/companies/#{company.id}")
    expect(page).to have_content("ESPN")
  end
  scenario "a user can see a company contact" do
    company = Company.create!(name: "ESPN")
    company.contacts.create!(full_name: "Nancy Jones", position: "HR Manager", email: "njones@hrsuxx.com")
    visit company_path(company)

    expect(page).to have_content("Nancy Jones")
    expect(page).to have_content("HR Manager")
    expect(page).to have_link("njones@hrsuxx.com")
  end
  scenario "a user can create a company contact" do
    company = Company.create!(name: "ESPN")

    visit company_path(company)

    fill_in "contact[full_name]", with: "Mary Smith"
    fill_in "contact[position]", with: "Secretary"
    fill_in "contact[email]", with: "msmith@company.com"
    click_on "Save"

    expect(current_path).to eq company_path(company)
    expect(page).to have_content "Mary Smith"
    expect(page).to have_content "Secretary"
    expect(page).to have_content "msmith@company.com"
  end
  scenario "there is a link to edit and delete" do
    company = Company.create!(name: "ESPN")
    company.contacts.create!(full_name: "Nancy Jones", position: "HR Manager", email: "njones@hrsuxx.com")

    visit company_path(company)

    expect(page).to have_link('Delete')
    expect(page).to have_link('Edit')
  end
  scenario "there is a link to edit and delete" do
    company = Company.create!(name: "ESPN")
    contact = company.contacts.create!(full_name: "Nancy Jones", position: "HR Manager", email: "njones@hrsuxx.com")

    visit company_path(company)

    click_link('Edit')

    expect(current_path).to eq(edit_contact_path(contact))

    fill_in 'contact[full_name]', with: 'Paul Paulson'
    fill_in 'contact[position]', with: 'HR Rep'
    fill_in 'contact[email]', with: 'email@email.com'
    click_on 'Save'

    expect(current_path).to eq(company_path(company))
    expect(page).to have_content('Paul Paulson')
    expect(page).to have_content('HR Rep')
    expect(page).to have_link('email@email.com')
  end
  scenario "there is a link to edit and delete" do
    company = Company.create!(name: "ESPN")
    company.contacts.create!(full_name: "Nancy Jones", position: "HR Manager", email: "njones@hrsuxx.com")

    visit company_path(company)

    click_link('Delete')

    expect(current_path).to eq(company_path(company))
  end
end
