require 'rails_helper'

describe 'user visits the company job page' do
  it 'should show only jobs for that company' do
    company = Company.create!(name: "ESPN")
    company_2 = Company.create!(name: "Netflix")
    category = Category.create(title: 'banana')
    job_1 = company.jobs.create!(title: "Developer", level_of_interest: 70, city: "Denver", category: category)
    job_2 = company_2.jobs.create!(title: "QA Analyst", level_of_interest: 70, city: "New York City", category: category)

    visit job_index_path(company)

    expect(page).to have_content(job_1.title)
    expect(page).to_not have_content(job_2.title)
  end
  it 'should link to the individual job' do
    company = Company.create!(name: "ESPN")
    company_2 = Company.create!(name: "Netflix")
    category = Category.create(title: 'banana')
    job_1 = company.jobs.create!(title: "Developer", level_of_interest: 70, city: "Denver", category: category)
    company_2.jobs.create!(title: "QA Analyst", level_of_interest: 70, city: "New York City", category: category)

    visit job_index_path(company)

    click_link 'Developer'

    expect(current_path).to eq(job_path(job_1))
  end
  it 'should link to the individual company' do
    company = Company.create!(name: "ESPN")
    company_2 = Company.create!(name: "Netflix")
    category = Category.create(title: 'banana')
    company.jobs.create!(title: "Developer", level_of_interest: 70, city: "Denver", category: category)
    company_2.jobs.create!(title: "QA Analyst", level_of_interest: 70, city: "New York City", category: category)

    visit job_index_path(company)

    click_link 'ESPN'

    expect(current_path).to eq(company_path(company))
  end
  it 'should link to the individual company' do
    company = Company.create!(name: "ESPN")
    company_2 = Company.create!(name: "Netflix")
    category = Category.create(title: 'banana')
    company.jobs.create!(title: "Developer", level_of_interest: 70, city: "Denver", category: category)
    company_2.jobs.create!(title: "QA Analyst", level_of_interest: 70, city: "New York City", category: category)

    visit job_index_path(company)
    click_link 'Denver'
    expect(page).to have_content('Denver Jobs')
  end

end
