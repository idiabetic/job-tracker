require 'rails_helper'

describe "User sees all jobs" do
  scenario "a user sees all the jobs for a specific company" do
    company = Company.create!(name: "ESPN")
    category = Category.create(title: 'banana')
    company.jobs.create!(title: "Developer", level_of_interest: 70, city: "Denver", category: category)
    company.jobs.create!(title: "QA Analyst", level_of_interest: 70, city: "New York City", category: category)

    visit company_path(company)

    expect(page).to have_content("ESPN")
  end
  scenario "a user sees jobs sorted by level of interest" do
    company = Company.create!(name: "ESPN")
    category = Category.create(title: 'banana')
    job_1 = company.jobs.create!(title: "Developer", level_of_interest: 70, city: "Denver", category: category)
    job_2 = company.jobs.create!(title: "QA Analyst", level_of_interest: 50, city: "LA", category: category)
    job_3 = company.jobs.create!(title: "DB Analyst", level_of_interest: 90, city: "New York City", category: category)

    visit jobs_path(sort:"interest")

    body = page.body
    expect(body.index(job_3.title) < body.index(job_1.title)).to eq true
    expect(body.index(job_1.title) < body.index(job_2.title)).to eq true
  end
  scenario "a user sees jobs sorted by city" do
    company = Company.create!(name: "ESPN")
    category = Category.create(title: 'banana')
    company.jobs.create!(title: "Developer", level_of_interest: 70, city: "Denver", category: category)
    company.jobs.create!(title: "QA Analyst", level_of_interest: 50, city: "Denver", category: category)
    job_3 = company.jobs.create!(title: "DB Analyst", level_of_interest: 90, city: "New York City", category: category)

    visit jobs_path(location:"Denver")

    expect(page).to_not have_content(job_3.title)
  end
end
