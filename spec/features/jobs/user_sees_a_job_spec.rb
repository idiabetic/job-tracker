require 'rails_helper'

describe "User sees a specific job" do
  scenario "a user sees a job for a specific company" do
    category = Category.create(title: 'Banana')
    company = Company.create!(name: "ESPN")
    job = company.jobs.create!(title: "Developer", level_of_interest: 70, city: "Denver", category_id: category.id)

    visit job_path(job)

    expect(page).to have_content("ESPN")
    expect(page).to have_content("Developer")
    expect(page).to have_content("70")
  end
  scenario 'a user can see all comments for a job' do
    category = Category.create(title: 'Banana')
    company = Company.create!(name: "ESPN")
    job = company.jobs.create!(title: "Developer", level_of_interest: 70, city: "Denver", category_id: category.id)
    comment_1 = job.comments.create!(body: 'Wow! Interview went very poorly.')
    comment_2 = job.comments.create!(body: 'Will follow up in 2 days anyway.')

    visit job_path(job)

    expect(page).to have_content(comment_1.body)
    expect(page).to have_content(comment_2.body)
  end

  scenario 'a user can add a comment for a job' do
    category = Category.create(title: 'Banana')
    company = Company.create!(name: "ESPN")
    job = company.jobs.create!(title: "Developer", level_of_interest: 70, city: "Denver", category_id: category.id)

    visit job_path(job)
    fill_in 'comment[body]', with: "Great chat with red head."

    click_on 'Create Comment'

    expect(current_path).to eq job_path(job)
    expect(page).to have_content("Great chat with red head.")
  end
end
