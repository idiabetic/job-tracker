require 'rails_helper'

describe "User creates a new job" do
  scenario "a user can create a new job" do
    Category.create!(title: 'Banana')
    company = Company.create!(name: "ESPN")

    visit new_job_path

    expect(page).to have_link('Add New Category')

    fill_in "job[title]", with: "Developer"
    fill_in "job[description]", with: "So fun!"
    select "Banana", from:  "job[category_id]"
    select "ESPN", from:  "job[company_id]"
    fill_in "job[level_of_interest]", with: 80
    fill_in "job[city]", with: "Denver"

    click_button "Create Job"

    expect(current_path).to eq("/jobs/#{Job.last.id}")
    expect(page).to have_content("ESPN")
    expect(page).to have_content("Developer")
    expect(page).to have_content("80")
    expect(page).to have_content("Denver")
  end
  it 'user should be redirected to add a new category' do
    Category.create!(title: 'Banana')
    company = Company.create!(name: "ESPN")

    visit new_job_path
    expect(page).to have_link('Add New Category')

    click_on('Add New Category')

    expect(current_path).to eq(new_category_path)
    expect(page).to have_content('Add New Category')
  end
end
