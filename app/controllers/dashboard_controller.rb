class DashboardController < ApplicationController
  def index
    @level_of_interest = Job.number_level_interest
    @top_companies = Job.top_companies
    @top_cities = Job.city_by_number
  end
end
