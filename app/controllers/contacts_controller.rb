class ContactsController < ApplicationController
  def create
    @contacts = Contact.new(contact_params)
    @contacts.company_id = params[:company_id]
    @contacts.save
    redirect_to company_path(params[:company_id])
  end

  def edit
    @contacts = Contact.find(params[:id])
  end

  def update
    @contact = Contact.find(params[:id])
    @contact.update(contact_params)
    redirect_to company_path(@contact.company)
  end

  def destroy
    contact = Contact.find(params[:id])
    Contact.destroy(params[:id])
    redirect_to company_path(contact.company)
  end


  private

  def contact_params
    params.require(:contact).permit(:full_name, :position, :email, :company_id)
  end
end
