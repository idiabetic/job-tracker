class CategoriesController < ApplicationController
  before_action :set_category, only: [:show, :edit, :update, :destroy]
  def index
    @categories = Category.all
    @job_counts = Job.category_by_number
  end

  def new
    @category = Category.new
  end

  def create
    if Category.find_by(category_params)
      flash.notice = "Sorry, #{category_params[:title]} already exists!"
      redirect_to new_category_path
    else
      @categories = Category.new(category_params)
      @categories.save
      redirect_to category_path(@categories)
    end
  end

  def show
    @jobs = Job.where(category_id: @category.id)
  end

  def edit
  end

  def update
    @category.update(category_params)
    redirect_to category_path(params[:id])
  end

  def destroy
    Category.destroy(@category.id)
    redirect_to categories_path
  end

  private
  def category_params
    params.require(:category).permit(:title)
  end

  def set_category
    @category = Category.find(params[:id])
  end
end
