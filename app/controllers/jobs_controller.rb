class JobsController < ApplicationController
  def index
    if params[:sort]
      @jobs = Job.sort_by(params[:sort])
      @title = 'All'
    elsif params[:location]
      @jobs = Job.restrict_by_city(params[:location])
      @title = params[:location]
    else
      @jobs = Job.all
      @title = 'All'
    end
  end

  def new
    @job = Job.new()
    @categories = Category.all
    @companies = Company.all
  end

  def create
    @company = Company.find(job_params[:company_id])
    @job = @company.jobs.new(job_params)
    if @job.save
      flash[:success] = "You created #{@job.title} at #{@company.name}"
      redirect_to job_path(@job)
    else
      render :new
    end
  end

  def show
    @job = Job.find(params[:id])
    @comment = Comment.new
    @comment.job_id = @job.id
  end

  def edit
    @job = Job.find(params[:id])
    @categories = Category.all
    @companies = Company.all
  end

  def update
    @job.update(job_params)
    redirect_to job_path(params[:id])
  end

  def destroy
    Job.destroy(params[:id])
    redirect_to jobs_path
  end

  private

  def job_params
    params.require(:job).permit(:title, :description, :level_of_interest, :city, :category_id, :company_id)
  end
end
