class Job < ApplicationRecord
  validates  :title, :level_of_interest, :city, :category_id, presence: true
  belongs_to :company
  belongs_to :category, optional: true
  has_many   :comments

  def self.sort_by(sort_params)
    if sort_params == 'interest'
      order('level_of_interest desc')
    else
      order('city asc')
    end
  end

  def self.restrict_by_city(city)
    where(city: city)
  end

  def self.number_level_interest
    group(:level_of_interest).count(:id)
  end

  def self.top_companies
    company_hash = group(:company).average(:level_of_interest)
    company_hash.sort_by { |_key, value| -value}[0..2]
  end

  def self.city_by_number
    group(:city).count(:id)
  end

  def self.category_by_number
    group(:category_id).count(:id)
  end
end
